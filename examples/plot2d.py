from dviplot.core import SimplePlot
from dvidraw.bijections import logspace, linspace, separable
from dvidraw.units import RawCoord
import random


from itertools import product

xlows = range(100)
xhighs = range(1, 101)

ylows = range(100)
yhighs = range(1, 101)

def color(x):
    return (x, x, x) if x < 0.9 else (1.0, 0.2, 0)

mesh = zip(product(xlows, ylows), product(xhighs, yhighs),
           (color((i) / 220.0) for i,j in product(xlows, ylows)))

import array

# Hardcode a unit histogram for now
class HistoData2D(object):
    def __init__(self, data, xticks=[], yticks=[], mapping=None):
        self.data = data
        self.x_range = (0, 100)
        self.y_range = (0, 100)
        self.xticks = [0] or yticks
        self.yticks = [0] or xticks
        self.mapping = mapping

    def draw(self, cr):
        cr.rel(*self.pos).to.move(*self.size).zoom()

        linlin = separable(linspace(*self.x_range),
                           linspace(*self.y_range), RawCoord, RawCoord)
        cr.spaces['plt'] = linlin.inverse + cr.spaces['box']
        cr.box(0, 0).to(1,1).rect()
        with cr.clip as cr:

            #data = array.array('B')
            for (x1, y1), (x2, y2), z in self.data:
                #r, g, b = z
                a = cr.plt(x1, y1).to(x2, y2).rect()
                # Yes, this is completely arbitrary!
                a.fill(cr.rgb(*z)).outline(color=cr.rgb(*z), weight=0.5)
                #.outline(color=None, size=0)#.fill(color=cr.rgb(z, z, z))
                #a = 255
                #data.extend((int(255 * b), int(255 * g), int(255 * r), a))

            #cr.box(0, 0).to(1, 1).image(data, len(xlows), len(ylows))
            
        # We *DEFINITELY* need some simplification for this!
        plt = cr.spaces['plt']
        cr.unzoom()
        cr.spaces['plt'] = plt

    @property
    def pos(self):
        return self.frame.pos

    @property
    def size(self):
        return self.frame.size

    
data = HistoData2D(mesh)

plot = SimplePlot(data, title="", xtitle="x", ytitle="q^2")
plot.size(317, 212)

d = plot.draw()
d.save('test.pdf')
d.save.screen()
